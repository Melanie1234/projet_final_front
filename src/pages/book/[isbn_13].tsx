
import { AuthContext } from "@/auth/auth-context";
import CartButton from "@/components/CartButton";
import WishListButton from "@/components/WishListButton";
import { Book } from "@/entities/Book";
import { deleteBook, fetchOneBook } from "@/services/book-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

export default function BookPage() {
    const router = useRouter();
    const { isbn_13, id } = router.query;
    const [book, setBook] = useState<Book>();
    const { token } = useContext(AuthContext);


    useEffect(() => {
        (async () => {
            if (!isbn_13) {
                return;
            }
            const data = await fetchOneBook(String(isbn_13));
            setBook(data);

        })();
    }, [isbn_13])

    async function remove() {
        await deleteBook(id);
        router.push('/');
    }

    if (!book) {
        return <p>Loading...</p>
    }

    return (
        <div className="card col-12 col-md-8 m-auto mb-5 mt-5 p-5">
            <Link className="nav-link" href="/" >
                <i className="bi bi-arrow-left-circle-fill" id="iconBack"></i>
            </Link>
            <div className="card-body row justify-content-around align-items-start">

                <div className="col-12 col-md-4 mb-3">
                    <img src={book.img} className="img-fluid rounded-start" alt={book.title} />
                </div>
                <div className="col-12 col-md-6 ms-md-5">
                    <h2 className="card-title mb-3">{book.title}</h2>
                    <h3 className="card-subtitle mb-5">{book.author}</h3>
                    <p className="card-text category">Catégorie : {book.genres}</p>
                    <p className="card-text category">Nombre de pages : {book.numberOfPages}</p>
                    <p className="card-text category">Date de parution : {book.publishDate}</p>
                    <p className="card-text category">Maison d'édition : {book.publishers}</p>
                    <p className="card-text content">Commentaire : {book.comment}</p>
                </div>

            </div>
            <div className="w-100 d-flex justify-content-center mt-5 mb-3">
                {token && <CartButton book={book} />}
                {token && <WishListButton book={book} />}
            </div>

        </div>
    );
}