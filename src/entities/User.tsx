
export interface User {

    id?: number,
    firstName?: string,
    lastName?: string,
    mail: string,
    address?: string,
    department?: string,
    city?: string,
    password: string,
    orders?: []
    score?: number
}