export interface Book {
    id?:number;
    isbn13: string;
    publishers:string;
    title:string;
    genres:string;
    numberOfPages: string;
    publishDate: string;
    author:string;
    comment: string;
    img: string;
    score?: string
}


