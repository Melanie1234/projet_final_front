
import React, { useEffect, useState } from "react";
import { User } from "@/entities/User";
import { fetchAllUsers } from "@/services/user-service";
import { formToJSON } from "axios";
import { log } from "console";

export default function AccountsList() {
    const [users, setUsers] = useState<User[]>([]);

    useEffect(() => {
        fetchAllUsers().then(data => {
            setUsers(data);
            console.log(data);
        });
    }, []);

    return (
        <div className="container">
            <div className="row">
                <h1 className="mb-5 mt-5">Tous les utilisateurs</h1>

                {users?.map((item: User) => (
                    <div key={item.id} className="card w-75 d-flex m-auto mb-2 mt-2">

                        <div className="mt-5 d-flex justify-content-around mb-5">
                            <div>
                                <p>Prénom : {item.firstName}</p>
                                <p>Nom : {item.lastName}</p>
                                <p>Mail : {item.mail}</p>
                            </div>

                            <div>
                                <p>Adresse : {item.address}</p>
                                <p>Code postal : {item.department}</p>
                                <p>Ville : {item.city}</p>
                                <p>Nombre de points : {item.score}</p>
                            </div>

                        </div>
                    </div>

                ))}
            </div>
        </div>
    );
}
