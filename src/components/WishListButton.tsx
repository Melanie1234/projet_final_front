
import { Book } from '@/entities/Book';
import { WishListContext } from '@/pages/context/wishList-context';
import React, { useContext, useState } from 'react';
import { Button, OverlayTrigger, Popover } from 'react-bootstrap';

interface Props {
  book:Book;
}

export default function WishListButton({book}:Props) {
  const [showPopover, setShowPopover] = useState(false);

  const handleButtonClick = () => {
    setShowPopover(true);
    setTimeout(() => {
      setShowPopover(false);
    }, 1500);
    addWishList()
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Ajouté à mes préférés</Popover.Header>
      <Popover.Body>Votre article est dans votre wishList</Popover.Body>
    </Popover>
  );

  const {books, addWish} = useContext(WishListContext);

  function addWishList () {
    addWish(book)
    console.log(books)
}

  return (
    <>

      <OverlayTrigger
        trigger="click"
        placement="top"
        overlay={popover}
        show={showPopover}
      >
       
        <Button
          className="custom-rounded-button m-2"
          style={{ color: '#F8E5A1', backgroundColor: '#F8E5A1', border: 'none' }}
          onClick={handleButtonClick}
        >
          <i className="bi bi-suit-heart-fill" style={{ color: 'white' }}></i>
        </Button>
      </OverlayTrigger>
    </>
  );
}


