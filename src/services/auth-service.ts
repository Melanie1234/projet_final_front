import { User } from "@/entities/User";
import axios from "axios";

export async function login(mail:string,password:string) {
    const response = await axios.post<{token:string}>('http://localhost:8000/api/login', {mail, password});
    console.log('from login')
    return response.data.token;
}

export async function postUser(user:User) {
    const response = await axios.post('http://localhost:8000/api/user' , user);
    return response.data;
}

export async function fetchUser() {
    const repsonse = await axios.get<User>('http://localhost:8000/api/account');
    return repsonse.data;
}

export async function updateUser(user:User) {
    const response = await axios.patch<User>('http://localhost:8000/api/user/'+user.id, user);
    return response.data;
}

