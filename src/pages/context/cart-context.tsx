import { useEffect, useState } from "react";
import { createContext } from "react";
import { setCookie, destroyCookie, parseCookies } from "nookies";
import { Book } from "@/entities/Book";


interface CartState {
    books:Book[],
    addBook : (value: Book) => void,
    removeBook : (value: Book) => void,
    clear: () => void
    
}

export const CartContext = createContext({} as CartState);

export const CartContextProvider = ({children}:any) => {
    const [books, setBooks] = useState<Book[]>([]);

    useEffect (() => {
        const { cart } = parseCookies(null, "cart");
        if (cart) {
            setBooks(JSON.parse(cart));
        }
    }, [])

    function addBook(value: Book) {
        if (!books.some((book) => book.id === value.id)) {
            const newBooks = [...books, value];
            setBooks(newBooks)
            setCookie(null, 'cart', JSON.stringify(newBooks))
        }
    }

    function removeBook(value: Book) {
        const updatedBooks = books.filter((book) => book.id !== value.id);
        setBooks(updatedBooks);
        setCookie(null, 'cart', JSON.stringify(updatedBooks));
        console.log(updatedBooks)
    }
    
    
    function clear() {
        setBooks([]);
        setCookie(null, 'cart', JSON.stringify([]));
    }

    return (
        <CartContext.Provider value={{books, addBook, removeBook, clear}}>
            {children}
        </CartContext.Provider>
    )

}