import { Book } from "@/entities/Book";
import axios from "axios";

export async function fetchOneBookFromApi(isbn_13:string) {
    const response = await axios.get<any>
    ('https://openlibrary.org/api/books?bibkeys=ISBN:'+isbn_13+'&jscmd=data&format=json');
    return response.data['ISBN:'+isbn_13];
}


export async function fetchOneBook(isbn_13:string) {
    const response = await axios.get<Book>('http://localhost:8000/api/book/'+isbn_13);
    return response.data;
}

export async function fetchByUser() {
    const response = await axios.get<Book[]>('http://localhost:8000/api/book/user/books');
    return response.data;
}

export async function fetchAllBooks() {
    const response = await axios.get<Book[]>('http://localhost:8000/api/book');
    return response.data;
}

export async function fetchSearch(search:string){
    const response = await axios.get<Book[]>('http://localhost:8000/api/book/search/'+search);
    return response.data;
}

export async function postBook(book:Book) {
    const response = await axios.post<Book>('http://localhost:8000/api/book', book);
    return response.data;
   
}


export async function deleteBook(id:any) {
    await axios.delete('http://localhost:8000/api/book/'+id);
}


