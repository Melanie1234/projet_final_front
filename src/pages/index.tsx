import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useEffect, useState } from "react";
import { Book } from "@/entities/Book";
import { fetchAllBooks } from "@/services/book-service";
import BookComponent from "@/components/BookComponent";

export default function Index() {
    const [books, setBooks] = useState<Book[]>([]);
    const [sortedBooks, setSortedBooks] = useState<Book[]>([]);
    const [selectedGenre, setSelectedGenre] = useState<string>("");


    useEffect(() => {
        fetchAllBooks().then(data => {
            setBooks(data);
            setSortedBooks(data);
        });
    }, []);

    useEffect(() => {
        if (selectedGenre === "") {
            setSortedBooks(books);
        } else {
            const filteredBooks = books.filter(book => book.genres.includes(selectedGenre));
            setSortedBooks(filteredBooks);
        }
    }, [selectedGenre, books]);

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12 mb-5 mt-5" >
                    <h2>Comment ça marche ?</h2>
                    <p id="parahgraphe">
                        Le principe est celui du troc !
                        Une fois que je suis inscrit.e, <span id="bold">je démarre avec un score de 5 points</span>. Je peux donc d'ores et déjà troquer un livre qui m'intéresse ! <br>
                        </br> Paralèlement, j'indique les livres que je possède en saisissant simplement leurs codes barres pour les mettre à disposition des autres utilisateurs.
                        <br></br>
                        Lorsqu'un.e membre souhaite un de mes livres, je le lui expédie et <span id="bold">je gagne 5 points</span> par livre.
                        Avec mes points je peux continuer à commander les livres que je désire. Je garde les livres que je reçois car ils sont à moi.
                    </p>
                    <p><span id="italic">Pas encore inscrit.e ?  <a href="/login">Inscrivez vous vite !</a> </span></p>
                </div>
            </div>

            <div className="row">
                <div className="col-md-12 mb-5 mt-3 pb-5">
                    <h2>Nouveaux livres à troquer</h2>

                    <label htmlFor="genreSelect">Trier par genre :</label>
                    <select
                        className="form-select "
                        id="genreSelect"
                        name="genres"
                        value={selectedGenre}
                        onChange={(e) => setSelectedGenre(e.target.value)}
                    >
                        <option value="">Tous les genres</option>
                        <option value="Romans">Romans</option>
                        <option value="Romans poche">Romans poche</option>
                        <option value="Romance">Romance</option>
                        <option value="Polar & Thriller">Polar & Thriller</option>
                        <option value="Fantasy & SF">Fantasy & SF</option>
                        <option value="BD & Humour">BD & Humour</option>
                        <option value="Manga">Manga</option>
                        <option value="Livres ados">Livres ados</option>
                        <option value="Livres enfants">Livres enfants</option>
                        <option value="Actualité Média et Société">Actualité Média et Société</option>
                        <option value="Dictionnaires & Langues">Dictionnaires & Langues</option>
                        <option value="Sciences Humaines">Sciences Humaines</option>
                        <option value="Histoire">Histoire</option>
                        <option value="Entreprise, Management">Entreprise, Management</option>
                        <option value="Poésie & Théâtre">Poésie & Théâtre</option>
                        <option value="Agendas et Calendriers">Agendas et Calendriers</option>
                        <option value="Art, Cinéma & Musique">Art, Cinéma & Musique</option>
                        <option value="Tourisme & Voyages">Tourisme & Voyages</option>
                        <option value="Santé & Bien-être">Santé & Bien-être</option>
                        <option value="Cuisine & Vins">Cuisine & Vins</option>
                        <option value="Sports & Loisirs">Sports & Loisirs</option>
                        <option value="Nature, Animaux & Jardin">Nature, Animaux & Jardin</option>
                    </select>
                </div>


                {sortedBooks.length === 0 && (
                    <div className="col-md-12">
                        <p>Aucun livre disponible dans cette catégorie.</p>
                    </div>
                )}
                {sortedBooks && sortedBooks.map(item => (
                    <div key={item.id} className="col-md-6 mb-3">
                        <BookComponent book={item} />
                    </div>
                ))}
            </div>
        </div>
    );
}