
import { AuthContext } from "@/auth/auth-context";
import { User } from "@/entities/User";
import { login, postUser } from "@/services/auth-service";
import { useRouter } from "next/router";
import { FormEvent, useContext, useState } from "react";

interface Props {
    from: string
}

export default function LoginForm({ from }: Props) {
    const router = useRouter();
    const { token, setToken } = useContext(AuthContext);
    const [error, setError] = useState('');
    const [log, setLog] = useState({
        mail: '',
        password: ''
    })

    const [userCreated, setUserCreated] = useState(false);
    const [agreedToTerms, setAgreedToTerms] = useState(false);


    const [user, setUser] = useState<User>(
        {
            firstName: '',
            lastName: '',
            mail: '',
            address: '',
            department: '',
            city: '',
            password: ''
        });


    const [fromPage, setFromPage] = useState(from);

    async function handleSuscribe(event: FormEvent) {
        event.preventDefault();
        try {
            await postUser(user);
            setUserCreated(true); 
            setUser({
                firstName: '',
                lastName: '',
                mail: '',
                address: '',
                department: '',
                city: '',
                password: ''
            }); 
            setTimeout(() => {
                setUserCreated(false); 
            }, 5000); 
        } catch (error) {
            console.error(error);
        }
    }

    function handleChange(event: any) {
        setLog({
            ...log,
            [event.target.name]: event.target.value
        })

        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event: any) {
        event.preventDefault();
        setError('');
        try {
            setToken(await login(log.mail, log.password));
            if (fromPage == "login") {

                router.push('/account');
            }
        } catch (error: any) {
            console.log(error)
            if (error.response?.status == 401) {
                setError('Identifiant/mot de passe invalide');
            } else {
                setError('Server error');
            }
        }
    }


    return (
        <>
            <div className="container-fluid">
                <div className="row mt-4">
                <div className="col-md-12 d-flex justify-content-around align-items-center">

                    {token ?

                        <button className="btn btn-danger logout-btn" onClick={() => setToken(null)}>Se déconnecter</button>
                        :
                    
                            <div className="col-md-6" id="subscribe">
                                {error && <p className="error-msg">{error}</p>}    
                                <div className="card p-5 mb-5">
                                <form onSubmit={handleSubmit}>
                                    <h2 className="mt-2">Se connecter</h2>

                                    <div className="mb-3">
                                        <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                                        <input type="mail" className="form-control" id="exampleInputEmail1" aria-describedby="mailHelp" name="mail" onChange={handleChange} required />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Mot de passe</label>
                                        <input type="password" className="form-control" id="exampleInputPassword1" name="password" onChange={handleChange} required />
                                    </div>

                                    <button type="submit" className="btn mt-5" id="btn">Se connecter</button>
                                </form>
                            </div>
                        </div>
                    }
                    
                        <div className="col-md-6" id="subscribe">
                            <div className="card p-5  mb-5">
                            {userCreated && (
                                <div className="alert alert-success" role="alert">
                                    Votre compte a été créé avec succès !
                                </div>
                            )}

                            <form onSubmit={handleSuscribe}>
                                <h2>Pas encore de compte ? Inscrivez vous !</h2>
                                <div className="mb-3">
                                    <label htmlFor="name" className="form-label ">Nom</label>
                                    <input type="text" name='firstName' value={user.firstName} onChange={handleChange} className="form-control" id="firstName" required />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="lastName" className="form-label ">Prénom</label>
                                    <input type="text" name='lastName' value={user.lastName} onChange={handleChange} className="form-control" id="lastName" required />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="email" className="form-label ">Email</label>
                                    <input type="text" name='mail' value={user.mail} onChange={handleChange} className="form-control" id="mail" required />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="address" className="form-label ">Adresse</label>
                                    <input type="text" name='address' value={user.address} onChange={handleChange} className="form-control" id="address" required />
                                </div>
                                    <div className="mb-3">
                                        <label htmlFor="phone" className="form-label ">Département</label>
                                        <input type="text" name='department' value={user.department} onChange={handleChange} className="form-control" id="department" required />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="phone" className="form-label ">Ville</label>
                                        <input type="text" name='city' value={user.city} onChange={handleChange} className="form-control" id="city" required />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label ">Mot de passe</label>
                                        <input type="password" name='password' value={user.password} onChange={handleChange} className="form-control" id="password" required />
                                    </div>
                                    <div className="form-check">
                                        <input
                                            className="form-check-input"
                                            type="checkbox"
                                            value=""
                                            id="flexCheckDefault"
                                            onChange={(e) => setAgreedToTerms(e.target.checked)}
                                        />
                                        <label className="form-check-label">
                                            J'accepte les <span id="conditions">conditions générales d'utilisation*</span>
                                        </label>

                                    </div>


                                    <button
                                        type="submit"
                                        className="btn bm-2 col-3 mt-5"
                                        id="btn"
                                        disabled={!agreedToTerms} 
                                    >
                                        S'inscrire
                                    </button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}