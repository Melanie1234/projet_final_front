

import { Book } from '@/entities/Book';
import { CartContext } from '@/pages/context/cart-context';
import React, { useContext, useState } from 'react';
import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';

interface Props {
  book: Book;
}

export default function CartButton({ book }: Props) {
  const [showPopover, setShowPopover] = useState(false);

  const handleButtonClick = () => {
    setShowPopover(true);
    setTimeout(() => {
      setShowPopover(false);
    }, 1500);
    addCart()
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Troquer</Popover.Header>
      <Popover.Body>Votre article est dans votre panier</Popover.Body>
    </Popover>
  );

  const { books, addBook } = useContext(CartContext);

  function addCart() {
    addBook(book)
    console.log(books)
  }


  return (
    <>
      <OverlayTrigger
        trigger="click"
        placement="top"
        overlay={popover}
        show={showPopover}
      >
        <Button id="btn" onClick={handleButtonClick}>
          Ajouter au panier
        </Button>
      </OverlayTrigger>
    </>
  );
}



