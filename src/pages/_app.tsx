import type { AppProps } from 'next/app'
import "@/styles/globals.css"
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import "@/auth/axios-config"
import Header from "@/components/HeaderComponent";
import Footer from "@/components/FooterComponent"
import { FormComponent } from '@/components/FormComponent';
import { AuthContextProvider } from '../auth/auth-context';
import { WishListContextProvider } from './context/wishList-context';
import { CartContextProvider } from './context/cart-context';
import { useEffect } from 'react';



export default function App({ Component, pageProps }: AppProps) {
useEffect(() => {
   require("bootstrap/dist/js/bootstrap.bundle.min.js")

  }, []);
  return (
    <>
      <AuthContextProvider>
        <CartContextProvider>

          <WishListContextProvider>
            <Header />
            <FormComponent />
            <Component {...pageProps} />
            <Footer />
          </WishListContextProvider>
        </CartContextProvider>

      </AuthContextProvider>
    </>
  );
}