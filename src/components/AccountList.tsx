

// import { AuthContext } from "@/auth/auth-context";
// import { User } from "@/entities/User";
// import { fetchUser, updateUser } from "@/services/auth-service";
// import { useRouter } from "next/router";
// import { useContext, useEffect, useState } from "react"

// interface Props {
//   from?: string,
//   user?: User
// }

// export default function AccountList({ from }: Props) {
//   const [fromPage, setFromPage] = useState(from);
//   const [user, setUser] = useState<User>
//     ({
//       firstName: '',
//       lastName: '',
//       mail: '',
//       address: '',
//       department: '',
//       city: '',
//       password: '',
//       score: 5,

//     });

//   const router = useRouter();
//   const { token, setToken } = useContext(AuthContext);

//   function logout() {
//     setToken(null)
//     if (fromPage == "account") {

//       router.push('/')
//     }
//   }

//   useEffect(() => {
//     fetchUser().then(data => {
//       setUser(data);
//     }).catch(error => {
//       if (error.response.status == 401) {
//         router.push('/login')
//       }
//     });
//   }, [])



//   function handleChange(event: any) {
//     setUser({
//       ...user,
//       [event.target.name]: event.target.value
//     });
//   }


//   async function fetchUpdateUser(event: any) {

//     event?.preventDefault()
//     const updated = await updateUser(user);
//     setUser(updated);
//     setShowEdit(!showEdit)
//   }

//   const [showEdit, setShowEdit] = useState(false);

//   const [showHistory, setShowHistory] = useState(false);

//   return (
//     <>
//       <div className="container-fluid">
//         <div className="row justify-content-center">

//           <div className="col-md-8">
//             <h1 className="mb-5 mt-5">Mon compte</h1>

//             <div className="mb-3 d-flex justify-content-around mb-5">
//               <div>
//                 <p>Prénom : {user.firstName}</p>
//                 <p>Nom : {user.lastName}</p>
//                 <p>Mail : {user.mail}</p>
//               </div>

//               <div>
//                 <p>Adresse : {user.address}</p>
//                 <p>Code postal : {user.department}</p>
//                 <p>Ville : {user.city}</p>
//                 <p>Nombre de points : {user.score}</p>
//               </div>

//             </div>

//             <div className="d-flex justify-content-around mb-5">

//               <button className="btn mt-2" id="btn" onClick={() => setShowEdit(!showEdit)}>Modifier</button>
//               <button className="btn logout-btn mt-2" id="btn" onClick={logout}>Se déconnecter</button>

//             </div>

//           </div>
//         </div>
//       </div>

//       {showEdit && <>
//         <div className="row justify-content-center">
//           <form onSubmit={fetchUpdateUser} className="col-md-6 m-4 p-3" id="form">

//             <div className="mb-3">
//               <label htmlFor="name" className="form-label ">Nom</label>
//               <input type="text" name='lastName' value={user.lastName} onChange={handleChange} className="form-control" id="firstName" required />
//             </div>
//             <div className="mb-3">
//               <label htmlFor="lastName" className="form-label ">Prénom</label>
//               <input type="text" name='firstName' value={user.firstName} onChange={handleChange} className="form-control" id="lastName" required />
//             </div>
//             <div className="mb-3">
//               <label htmlFor="email" className="form-label ">Email</label>
//               <input type="text" name='mail' value={user.mail} onChange={handleChange} className="form-control" id="mail" required />
//             </div>
//             <div className="mb-3">
//               <label htmlFor="address" className="form-label ">Adresse</label>
//               <input type="text" name='address' value={user.address} onChange={handleChange} className="form-control" id="address" required />
//             </div>
//             <div className="mb-3">
//               <label htmlFor="phone" className="form-label ">Département</label>
//               <input type="text" name='department' value={user.department} onChange={handleChange} className="form-control" id="department" required />
//             </div>

//             <div className="mb-3">
//               <label htmlFor="phone" className="form-label ">Ville</label>
//               <input type="text" name='city' value={user.city} onChange={handleChange} className="form-control" id="city" required />
//             </div>

//             <div className="row justify-content-center">

//               <button type="submit" id="btn" className="btn btn-success col-3" >Valider</button>
//             </div>

//           </form>
//         </div>



//       </>}

//     </>

//   )
// }







import { AuthContext } from "@/auth/auth-context";
import { User } from "@/entities/User";
import { fetchUser, updateUser } from "@/services/auth-service";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react"

interface Props {
  from?: string,
  user?: User
}

export default function AccountList({ from }: Props) {
  const [fromPage, setFromPage] = useState(from);
  const [user, setUser] = useState<User>
    ({
      firstName: '',
      lastName: '',
      mail: '',
      address: '',
      department: '',
      city: '',
      password: '',
      score: 5,

    });
    

  const router = useRouter();
  const { token, setToken } = useContext(AuthContext);

  function logout() {
    setToken(null)
    if (fromPage == "account") {

      router.push('/')
    }
  }

  useEffect(() => {
    fetchUser().then(data => {
      setUser(data);
    }).catch(error => {
      if (error.response.status == 401) {
        router.push('/login')
      }
    });
  }, [])



  function handleChange(event: any) {
    setUser({
      ...user,
      [event.target.name]: event.target.value
    });
  }


  async function fetchUpdateUser(event: any) {

    event?.preventDefault()
    const updated = await updateUser(user);
    setUser(updated);
    setShowEdit(!showEdit)
  }

  const [showEdit, setShowEdit] = useState(false);

  const [showHistory, setShowHistory] = useState(false);

  return (
    <>
      <div className="container-fluid">
        <div className="row justify-content-center">

          <div className="col-md-8">
            <h1 className="mb-5 mt-5">Mon compte</h1>

            <div className="mb-3 d-flex justify-content-around mb-5">
              <div>
                <p>Prénom : {user.firstName}</p>
                <p>Nom : {user.lastName}</p>
                <p>Mail : {user.mail}</p>
              </div>

              <div>
                <p>Adresse : {user.address}</p>
                <p>Code postal : {user.department}</p>
                <p>Ville : {user.city}</p>
                <p>Nombre de points : {user.score}</p>
              </div>

            </div>

            <div className="d-flex justify-content-around mb-5">

              <button className="btn mt-2" id="btn" onClick={() => setShowEdit(!showEdit)}>Modifier</button>
              <button className="btn logout-btn mt-2" id="btn" onClick={logout}>Se déconnecter</button>

            </div>

          </div>
        </div>
      </div>

      {showEdit && <>
        <div className="row justify-content-center">
          <form onSubmit={fetchUpdateUser} className="col-md-6 m-4 p-3" id="form">

            <div className="mb-3">
              <label htmlFor="name" className="form-label ">Nom</label>
              <input type="text" name='lastName' value={user.lastName} onChange={handleChange} className="form-control" id="firstName" required />
            </div>
            <div className="mb-3">
              <label htmlFor="lastName" className="form-label ">Prénom</label>
              <input type="text" name='firstName' value={user.firstName} onChange={handleChange} className="form-control" id="lastName" required />
            </div>
            <div className="mb-3">
              <label htmlFor="email" className="form-label ">Email</label>
              <input type="text" name='mail' value={user.mail} onChange={handleChange} className="form-control" id="mail" required />
            </div>
            <div className="mb-3">
              <label htmlFor="address" className="form-label ">Adresse</label>
              <input type="text" name='address' value={user.address} onChange={handleChange} className="form-control" id="address" required />
            </div>
            <div className="mb-3">
              <label htmlFor="phone" className="form-label ">Département</label>
              <input type="text" name='department' value={user.department} onChange={handleChange} className="form-control" id="department" required />
            </div>

            <div className="mb-3">
              <label htmlFor="phone" className="form-label ">Ville</label>
              <input type="text" name='city' value={user.city} onChange={handleChange} className="form-control" id="city" required />
            </div>

            <div className="row justify-content-center">

              <button type="submit" id="btn" className="btn btn-success col-3" >Valider</button>
            </div>

          </form>
        </div>



      </>}

    </>

  )
}




