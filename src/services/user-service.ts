import { User } from "@/entities/User";
import axios from "axios";

export async function fetchAllUsers(){
    const response = await axios.get<User[]>('http://localhost:8000/api/user/admin');
    return response.data;
}
