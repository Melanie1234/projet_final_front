
export interface Order {
    id?:number,
    date?: string,
    deliveryMode:string,
    status?: string,
    user_id?: number,
    book_id?:number
}

