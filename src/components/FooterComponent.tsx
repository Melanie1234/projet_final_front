export default function footerComponent() {

    return (
        <footer>
            <nav className="navbar" id="footer">
                <span className="navbar-text footer m-auto text-center"> 
                    <a className="navbar-brand" href="/" >
                        <img src="/img/Logo.png" alt="logo" style={{ width: "25%" }} />
                    </a>
                </span>
            </nav>
        </footer>


    )
}