import { Book } from "@/entities/Book";
import WishListButton from "./WishListButton";
import { Button } from "react-bootstrap";
import CartButton from "./CartButton";
import { useContext } from "react";
import { AuthContext } from "@/auth/auth-context";

interface Props {
  book: Book;
  hideWishListButton?: boolean;
  hideCartButton?: boolean;

  children?: React.ReactNode;
}

export default function BookComponent({ book, hideWishListButton, hideCartButton }: Props) {
  const { token } = useContext(AuthContext);

  return (
    <div className="card w-75 d-flex m-auto mb-2 mt-2" id="bookCard">
      <div className="row g-0">
        <div className="col-md-6">
          <img src={book.img} className="img-fluid rounded-start h-100" alt={book.title} />
        </div>
        <div className="col-md-6">
          <div className="card-body h-100 " id="bookCardComponent">
            <div>
              <h3 className="card-title">{book.title}</h3>
              <h3 className="card-text">{book.author}</h3>
              <p className="card-text">Nombre de points : {book.score}</p>
            </div>
            <div className="mt-auto mb-3">
              {token &&  !hideWishListButton && <WishListButton book={book} />}
            </div>
            <div className="d-flex flex-column justify-content-end align-items-start"> 
              <Button href={"/book/" + book.isbn13} className="card-link link mb-3" id="btn">
                Lire plus
              </Button>
              {token && !hideCartButton && (
                  <CartButton book={book} />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
