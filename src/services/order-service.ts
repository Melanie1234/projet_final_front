import { Order } from "@/entities/Order";
import axios from "axios";

export async function postOrder(order:Order){
    const response = await axios.post<Order>('/api/order', order);
    return response.data;
}

