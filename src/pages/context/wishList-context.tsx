
import { useEffect, useState } from "react";
import { createContext } from "react";
import { setCookie, destroyCookie, parseCookies } from "nookies";
import { Book } from "../../entities/Book";

interface WishListState {
  books: Book[];
  addWish: (value: Book) => void;
  removeBook: (value: Book) => void;
}

export const WishListContext = createContext({} as WishListState);

export const WishListContextProvider = ({ children }: any) => {
  const [books, setBooks] = useState<Book[]>([]);

  useEffect(() => {
    const { wishList } = parseCookies(null, "wishList");
    if (wishList) {
      setBooks(JSON.parse(wishList));
    }
  }, []);

  function addWish(value: Book) {
    if (!books.some((book) => book.id === value.id)) {
      const newBooks = [...books, value];
      setBooks(newBooks);
      setCookie(null, "wishList", JSON.stringify(newBooks));
    }
  }

  function removeBook(value: Book) {
    const updatedBooks = books.filter((book) => book.id !== value.id);
    setBooks(updatedBooks);
    setCookie(null, "wishList", JSON.stringify(updatedBooks));
  }

  return (
    <WishListContext.Provider value={{ books, addWish, removeBook }}>
      {children}
    </WishListContext.Provider>
  );
};
