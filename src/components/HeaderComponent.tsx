
import jwtDecode from "jwt-decode";
import Link from "next/link"
import { useContext, useState } from "react";
import router from "next/router";
import { Book } from "@/entities/Book";
import { AuthContext } from "@/auth/auth-context";

export default function Header() {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState<Book[]>([]);
  const { token } = useContext(AuthContext);


  const handleSearch = async (event: any) => {
    event.preventDefault();
    const searchTerm = event.target.elements.search.value;
    const isEmptyOrSpaces = /^\s*$/.test(searchTerm);
    if (!isEmptyOrSpaces) {
      setSearchQuery(searchTerm);
      router.push('/search/' + searchTerm);
    }
  };

  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
      return false
    }
  }



  return (
    <nav className="navbar navbar-expand-lg">
      <div className="container-fluid header">
        <div className="d-flex align-items-center justify-content-between">
          <a className="navbar-brand" href="/">
            <img src="/img/Logo.png" alt="logo" style={{ width: "25%" }} />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
            id="toggle"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <form
            className="d-flex navbar-nav mb-2 mb-lg-0"
            role="search"
            onSubmit={handleSearch}
          >
            <div className="d-flex flex-column flex-lg-row align-items-center">
              <input
                id="iconMenu"
                type="search"
                className="form-control me-2"
                placeholder="Rechercher un livre"
                aria-label="Search"
                value={searchQuery}
                onChange={(e) => setSearchQuery(e.target.value)}
                style={{
                  width: "100%",
                  maxWidth: "75%",
                }}
                name="search"
              />
              <button
                className="btn btn-outline-success"
                type="submit"
                id="btn"
              >
                Rechercher
              </button>
            </div>
          </form>

          <ul className="navbar-nav ms-auto mb-2 mb-lg-0 d-flex ">
            <div className="d-flex justify-content-around">
              <li className="nav-item">
                <Link className="nav-link" href={token ? "/account" : "/login"}>
                  <i className="bi bi-person" id="iconMenu"></i>
                </Link>
              </li>
              {!isAdmin() && token && (
                <>
                  <li className="nav-item">
                    <Link className="nav-link" href="/library">
                      <i className="bi bi-book" id="iconMenu"></i>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" href="/wishList">
                      <i className="bi bi-suit-heart" id="iconMenu"></i>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" href="/cart">
                      <i className="bi bi-cart4" id="iconMenu"></i>
                    </Link>
                  </li>
                </>
              )}

              {isAdmin() && (
                <li className="nav-item">
                  <Link className="nav-link" href="/accountsList">
                    <i className="bi bi-person-lines-fill" id="iconMenu"></i>
                  </Link>
                </li>
              )}
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}  

