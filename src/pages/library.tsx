
// import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
// import { useEffect, useState } from "react";
// import { Book } from "@/entities/Book";
// import { fetchByUser } from "@/services/book-service";
// import BookComponent from "@/components/BookComponent";

// export default function Library() {
//     const [books, setBooks] = useState<Book[]>([]);
//     useEffect(() => {
//         fetchByUser().then(data => {
//             setBooks(data);
//         });
//     }, []);

//     return (
//         <div className="container">
//             <div className="row">
//                 <h1 className="mb-5 mt-5">Ma bibliothèque</h1>
                
//                 {books.map(item => (
//                     <div key={item.id} className="col-md-6 mb-3">
//                         <BookComponent book={item} hideCartButton={true} hideWishListButton={true} />
//                     </div>
//                 ))}
//             </div>
//         </div>
//     );
// }


import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useEffect, useState } from "react";
import { Book } from "@/entities/Book";
import { fetchByUser } from "@/services/book-service";
import BookComponent from "@/components/BookComponent";

export default function Library() {
    const [books, setBooks] = useState<Book[]>([]);
    
    useEffect(() => {
        fetchByUser().then(data => {
            setBooks(data);
        });
    }, []);

    return (
        <div className="container">
            <div className="row">
                <h1 className="mb-5 mt-5">Ma bibliothèque</h1>
                
                {books.length === 0 ? (
                    <p>Vous n'avez pas encore de bibliothèque. Ajoutez vite vos livres !</p>
                ) : (
                    books.map(item => (
                        <div key={item.id} className="col-md-6 mb-3">
                            <BookComponent book={item} hideCartButton={true} hideWishListButton={true} />
                        </div>
                    ))
                )}
            </div>
        </div>
    );
}


