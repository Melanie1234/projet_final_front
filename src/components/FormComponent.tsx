import { AuthContext } from "@/auth/auth-context"
import { Book } from "@/entities/Book"
import { fetchOneBookFromApi, postBook } from "@/services/book-service"
import { useRouter } from "next/router"
import { FormEvent, useContext, useState } from "react"

export const FormComponent = () => {
    const router = useRouter()
    const { token } = useContext(AuthContext);

    const [book, setBook] = useState<Book>(

        {
            id: 1, isbn13: '', publishers: '', title: '', genres: '',
            numberOfPages: '', publishDate: '', author: '', comment: '',
            img: ''
        }
    )

    const [isBookAdded, setIsBookAdded] = useState(false);

    const handleCollapseToggle = () => {
        const collapseElement = document.getElementById("collapseExample");
        if (collapseElement) {
            const isCollapsed = collapseElement.classList.contains("show");
            if (isCollapsed) {
                collapseElement.classList.remove("show");
            } else {
                collapseElement.classList.add("show");
            }
        }
    };


    function handleChange(event: any) {
        setBook({
            ...book,
            [event.target.name]: event.target.value
        });
    }

    async function handleBlur(event: React.FocusEvent<HTMLInputElement>) {
        console.log('que');

        const { name, value } = event.target;

        if (name === 'isbn13' && value !== book.isbn13 && value.length == 13) {

            try {
                const added = await fetchOneBookFromApi(value);

                const extractedData: Partial<Book> = {
                    publishers: added.publishers?.[0]?.name || '',
                    title: added.title,
                    genres: added.genres,
                    numberOfPages: added.number_of_pages || '',
                    publishDate: added.publish_date || '',
                    author: added.authors?.[0]?.name || '',
                    comment: added.comment || '',
                    img: added.cover?.large || '',
                };

                setBook(prevBook => ({
                    ...prevBook,
                    ...extractedData,
                    isbn13: value
                }));

            } catch (error) {
                console.log("Error fetching book data:", error);
            }
        } else {
            setBook(prevBook => ({
                ...prevBook,
                [name]: value
            }));
        }
    }
    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        if (token) {
            try {
                const added = await postBook(book);
                console.log(added);
                setIsBookAdded(true); 
                setBook({
                    id: 1,
                    isbn13: '',
                    publishers: '',
                    title: '',
                    genres: '',
                    numberOfPages: '',
                    publishDate: '',
                    author: '',
                    comment: '',
                    img: ''
                }); 
            } catch (error) {
                console.error("Error adding book:", error);
            }
        } else {
            router.push('/signup');
        }
    }
    

    return (
        <>
            {token && (
                <p className="d-inline-flex gap-1 mt-5 ms-5 mb-5">
                    <button
                        className="btn"
                        id="btn"
                        type="button"
                        onClick={handleCollapseToggle}
                    >
                        Ajouter un livre à votre bibliothèque !
                    </button>
                </p>
            )}
             {token && (
            <div className="collapse " id="collapseExample">
                <div className="card card-body border border-0">
                  
                    <form onSubmit={handleSubmit} className="card w-50 m-auto p-5 mt-5 mb-5">  {isBookAdded ? (
                        <div className="alert alert-success" role="alert">
                            Votre livre à bien été ajouté à votre bibliothèque !
                        </div>
                    ) : null}
                        <h2 className="mb-5">Ajouter un livre</h2>
                        <p id="parahgraphe" className="mb-5">Entrer le <span id="bold">code barre</span> du livre que vous voulez ajouter, le reste des informations devrait être complété automatiquement.
                            Néamoins, vous pouvez modifier ces informations à tout moment !</p>

                        <div className="mb-3">
                            <label className="form-label"> Code bar </label>
                            <input type="text" value={book.isbn13} className="form-control" name="isbn13" onChange={handleChange} onInput={handleBlur} />
                        </div>

                        <div className="mb-3">
                            <label className="form-label"> Titre </label>
                            <input type="text" value={book.title} className="form-control" name="title" onChange={handleChange} />
                        </div>

                        <div className="mb-3">
                            <label className="form-label"> Editeur </label>
                            <input type="text" value={book.publishers} className="form-control" name="publishers" onChange={handleChange} />
                        </div>

                        <div className="mb-3">
                            <label className="form-label"> Date de parution </label>
                            <input type="text" value={book.publishDate} className="form-control" name="publish_date" onChange={handleChange} />
                        </div>

                        <div className="mb-3">
                            <label className="form-label"> Nombre de pages </label>
                            <input type="text" value={book.numberOfPages} className="form-control" name="number_of_pages" onChange={handleChange} />
                        </div>

                        <div className="mb-3">
                            <label className="form-label"> Auteur </label>
                            <input type="text" value={book.author} className="form-control" name="author" onChange={handleChange} />
                        </div>

                        <div className="mb-3">
                            <label className="form-label"> Commentaire </label>
                            <input type="text" value={book.comment} className="form-control" name="comment" onChange={handleChange} />
                        </div>

                        <div className="mb-3 mb-5">
                            <label className="form-label"> Photo de couverture </label>
                            <input type="text" value={book.img} className="form-control" name="img" onChange={handleChange} />
                        </div>

                        <div className="col-sm-3 mb-5 w-50">
                            <label  htmlFor="specificSizeSelect">Catégories</label>
                            <select className="form-select" id="specificSizeSelect" name="genres" value={book.genres} onChange={handleChange}>
                                <option selected>Choisir une catégorie...</option>
                                <option value="Romans">Romans</option>
                                <option value="Romans Poche">Romans Poche</option>
                                <option value="Romance">Romance</option>
                                <option value="Polar & Thriller">Polar & Thriller</option>
                                <option value="Fantasy & SF">Fantasy & SF</option>
                                <option value="BD & Humour">BD & Humour</option>
                                <option value="Manga">Manga</option>
                                <option value="Livres ados">Livres ados</option>
                                <option value="Livres enfants">Livres enfants</option>
                                <option value="Actualité Média et Société">Actualité Média et Société</option>
                                <option value="Dictionnaires & Langues">Dictionnaires & Langues</option>
                                <option value="Sciences Humaines">Sciences Humaines</option>
                                <option value="Histoire">Histoire</option>
                                <option value="Entreprise, Management">Entreprise, Management</option>
                                <option value="Poésie & Théâtre">Poésie & Théâtre</option>
                                <option value="Agendas et Calendriers">Agendas et Calendriers</option>
                                <option value="Art, Cinéma & Musique">Art, Cinéma & Musique</option>
                                <option value="Tourisme & Voyages">Tourisme & Voyages</option>
                                <option value="Santé & Bien-être">Santé & Bien-être</option>
                                <option value="Cuisine & Vins">Cuisine & Vins</option>
                                <option value="Sports & Loisirs">Sports & Loisirs</option>
                                <option value="Nature, Animaux & Jardin">Nature, Animaux & Jardin</option>
                            </select>
                        </div>
                        <button className="btn" id="btn">Ajouter ce livre</button>
                    </form>
                </div>
            </div>
                      )}
        </>

    )


}


