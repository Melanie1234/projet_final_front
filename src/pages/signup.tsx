import { User } from "@/entities/User";
import { postUser } from "@/services/auth-service";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react"



export default function signup() {
    const [errors, setErrors] = useState('');
    const router = useRouter();
    const [user, setUser] = useState<User>(
        {
            firstName: '',
            lastName: '',
            mail: '',
            address: '',
            department: '',
            city: '',
            password: ''

        });


    function handleChange(event: any) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();

        try {
            postUser(user);
            router.push('/login');

        } catch (error: any) {
            if (error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }

    return (
        <>
            <h2 className="mt-2">S'inscrire</h2>
            <div className="row justify-content-center">
                <form onSubmit={handleSubmit} className="col-md-6 m-4 p-3" id="form">

                    <div className="mb-3">
                        <label htmlFor="name" className="form-label ">Nom</label>
                        <input type="text" name='firstName' value={user.firstName} onChange={handleChange} className="form-control" id="firstName" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="lastName" className="form-label ">Prénom</label>
                        <input type="text" name='lastName' value={user.lastName} onChange={handleChange} className="form-control" id="lastName" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label ">Email</label>
                        <input type="text" name='mail' value={user.mail} onChange={handleChange} className="form-control" id="mail" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="address" className="form-label ">Adresse</label>
                        <input type="text" name='address' value={user.address} onChange={handleChange} className="form-control" id="address" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="phone" className="form-label ">Département</label>
                        <input type="text" name='department' value={user.department} onChange={handleChange} className="form-control" id="department" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="phone" className="form-label ">Ville</label>
                        <input type="text" name='city' value={user.city} onChange={handleChange} className="form-control" id="city" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label ">Mot de passe</label>
                        <input type="password" name='password' value={user.password} onChange={handleChange} className="form-control" id="password" required />
                    </div>
                    <button type="submit" className="btn btn-success m-2 col-3 btn-lg">S'inscrire</button>

                </form>
            </div>
        </>
    )

}
