import { useContext, useEffect, useState } from "react"
import { parseCookies } from "nookies";
import AccountList from "@/components/AccountList";
import { CartContext } from "./context/cart-context";
import { AuthContext } from "../auth/auth-context";
import { Order } from "@/entities/Order";
import { Book } from "@/entities/Book";
import { postOrder } from "@/services/order-service";
import LoginForm from "@/components/LoginForm";


export default function Cart() {

    const { books, removeBook, clear } = useContext(CartContext);

    const { token, setToken } = useContext(AuthContext);

    const [order, setOrder] = useState<Order>({
        deliveryMode: "A domicile"
    })

    function totalCount() {
        let total = 0;
        books.map((item: Book) =>
            total += Number(item.score)
        );
        return total
    }

    useEffect(() => {
        console.log(books);
    }, [books]);


    const [step, setStep] = useState(0);

    function confirmCart() {
        postOrder(order);
        setStep(3);
        console.log(order);
        clear();
    }

    function handleChange(event: any) {
        setOrder({
            ...order,
            [event.target.name]: event.target.value
        })
    }


    return (
        <>
            <div className="container-fluid d-flex justify-content-center ">
                <div className="container-fluid">
                    <h2>Mon panier</h2>
                    <div className="row d-flex justify-content-center mt-5">


                        {step < 1 && totalCount() > 0 &&
                            <div className="col-sm-8 col-md-6 mb-3">

                                <ul className="list-group">

                                    {books?.map((item: Book) =>

                                        <li className="list-group-item" key={item.id}>
                                            <div className="row">
                                                <div className="col-8">
                                                    <p key={item.id}>{item.title}</p>
                                                    <p>{item.author}</p>
                                                    <p>Score : {item.score}</p>
                                                    <div className="cart-btns">

                                                        <button className="btn search-btn me-1" id="btn" onClick={() => removeBook(item)}><i className="bi bi-trash3"></i></button>
                                                        <button className="btn search-btn" id="btn"><i className="bi bi-suit-heart"></i></button>
                                                    </div>

                                                </div>

                                            </div>
                                        </li>

                                    )}
                                    <li className="list-group-item text-center">
                                        <p>SOUS-TOTAL : {totalCount()} points</p>
                                    </li>
                                </ul>
                            </div>
                        }


                        <div className="total border rounded bg-white col-sm-8 col-md-6">
                            {totalCount() == 0 && step < 3 && <p className="text-center">Votre panier est vide</p>}

                            {step === 0 && totalCount() > 0 &&
                                <>
                                    <p>Sous total : {totalCount()} points</p>
                                    <label htmlFor="price" className="mt-2">Mode de livraison :</label>
                                    <select className="form-select mt-3" aria-label="price" id="price" name="deliveryMode" onChange={handleChange}>
                                        <option value="A domicile" >Livraison à domicile</option>
                                        <option value="Point relai" >Livraison en point relai</option>
                                    </select>
                                    <div className="row justify-content-center mt-3 mb-3">
                                        <button className="col-5  btn btn-success" onClick={() => setStep(1)}>VALIDER LE PANIER</button>
                                    </div>
                                </>
                            }
                            {step === 1 && token && <>
                                <h4 className="text-center">Vérifiez vos informations</h4>
                                <AccountList from="cart" />
                                <div className="row justify-content-center mt-3 mb-3">
                                    <button className=" col-5 btn btn-success" onClick={() => setStep(2)}>Continuer</button>
                                </div>
                            </>
                            }
                            {step === 1 && !token && <><h4>Connectez vous ou créez un compte</h4> <LoginForm from="cart" /></>}
                            {step === 2 && <>
                                <h4 className="text-center">Récapitulatif de commande</h4>

                                <ul className="list-group">

                                    {books?.map((item: Book) =>

                                        <li className="list-group-item" key={item.id}>
                                            <div className="row">
                                                <div className="col-8">
                                                    <h5 key={item.id}>{item.title}</h5>
                                                    <p>{item.author} </p>
                                                    <p>Score : {item.score}</p>
                                                </div>

                                            </div>
                                        </li>
                                    )}
                                    <li className="list-group-item text-center">
                                        <p>TOTAL : {totalCount()} €</p>
                                    </li>
                                </ul>
                                <div className="row justify-content-center mt-3 mb-3">
                                    <button className="col-5 btn btn-success" onClick={confirmCart}>Valider la commande </button>
                                </div>
                            </>
                            }
                            {step === 3 && <h4 className="text-center">Commande validée</h4>}

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}