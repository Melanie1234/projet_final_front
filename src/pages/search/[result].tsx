import React, { useState, useEffect } from 'react';
import { Book } from '@/entities/Book';
import BookComponent from '@/components/BookComponent';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import { fetchSearch } from '@/services/book-service';

interface Props {
  books: Book[];
}

const ResultPage: React.FC<Props> = ({ books }: Props) => {
  const router = useRouter();
  const { result } = router.query;

  const [formValue, setFormValue] = useState<string>(result as string);
  return (
    <>

      <div>
        {books.length > 0 ? (
          books.map((book) => (
            <BookComponent key={book.id} book={book} />
          ))
        ) : (
          <p>Désolé, aucun livre n'a été trouvé.</p>
        )}
      </div>
    </>
  );
}
export default ResultPage;

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
  const { result } = context.query;

  return {
    props: {
      books: await fetchSearch(String(result)),
    }
  }
}

