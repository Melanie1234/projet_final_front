
import { useContext } from "react"
import { Book } from "@/entities/Book";
import { WishListContext } from "./context/wishList-context";
import BookComponent from "@/components/BookComponent";


export default function WishList() {

    const { addWish, removeBook, books } = useContext(WishListContext);

    return (
        <>
            <div className="container">

                <div className="row ">
                    <h1>Ma wishList</h1>
                    {books?.map((item: Book) =>

                        <div key={item.id} className="col-md-6 mb-3">
                            <BookComponent book={item} hideWishListButton={true} />
                        </div>
                    )}

                </div>
            </div>
        </>
    )
}
